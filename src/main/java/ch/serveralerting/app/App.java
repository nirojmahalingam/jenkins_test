package ch.serveralerting.app;
import org.hibernate.Session;
import ch.serveralerting.util.HibernateUtil;
/**
 * Created by niroj on 09.07.2017.
 */
public class App {
    /**
     * App Main Function
     *
     * @param  args
     * @return void
     **/
    public static void main(String[] args) {
        System.out.println("GRADLE + Hibernate + HSQL");

        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();


        session.close();

    }
}

